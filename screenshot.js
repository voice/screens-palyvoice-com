var page = require('webpage').create();
var system = require('system');
var color = require('bash-color');
var fs = require('fs');

// you need to call this with args (phantomjs screenshot.js palyvoice.com)
var siteName = system.args[1];
var siteURL = 'http://'+siteName;
var now = new Date();

page.viewportSize = {
    width: 1366,
    height: 600
};

page.open(siteURL, function () {
    console.log(color.cyan('adding site ')+siteName);
    
    fs.makeDirectory('www');
    fs.changeWorkingDirectory('www');
    fs.makeDirectory('assets');
    fs.changeWorkingDirectory('assets');
    fs.makeDirectory(siteName);
    fs.changeWorkingDirectory(siteName);

    var date = ('0' + now.getDate()).slice(-2);
    var month = ('0' + (now.getMonth()+1)).slice(-2);
    var year = now.getFullYear();

    var fileName = siteName + '-' + date + '-' + month + '-' + year;

    page.render(fileName+'.png');
    console.log(color.green('saving screenshot ')+fileName+'.png');
    phantom.exit();
});
