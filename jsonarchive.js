var system = require('system');
var color = require('bash-color');
var fs = require('fs');
var $ = require('http');

// you need to call this with args (phantomjs screenshot.js palyvoice.com)
var siteName = system.args[2];
var siteURL = 'http://'+siteName;
var now = new Date();

var options = {
    host: siteName,
    path: '/feed/json'
};

console.log(JSON.stringify(options));

var date = ('0' + now.getDate()).slice(-2);
var month = ('0' + (now.getMonth()+1)).slice(-2);
var year = now.getFullYear();

var fileName = siteName + '-' + date + '-' + month + '-' + year;

$.request(options, function (response) {
    var str = '';
    
    response.on('data', function (chunk) {
	str += chunk;
    });
    
    response.on('end', function () {
	console.log(color.cyan('fetching stories ')+siteName);

	fs.mkdir('www/assets/'+siteName, function (err) { }); // directory exists, do nothing
	process.chdir('www/assets/'+siteName);

	fs.writeFile(fileName+'.json', str, function (err) {
	    if (err) console.log(color.red("error writing json: ")+err);
	    else console.log(color.green("saving file ")+fileName+'.json');
	});
    });
}).end();
