<?php
function get_file_list ($dir_path) {
  $dir = opendir("assets/$dir_path");
  $files = Array();

  /* This is the correct way to loop over the directory. */
  // goddamn it chris
  while (false !== ($entry = readdir($dir))) {
    if (pathinfo($entry, PATHINFO_EXTENSION) == "png") {
      array_push($files, $entry);
    }
  }

  return $files;
}

function get_date_list ($files) {
  $dates = Array();

  foreach ($files as &$file) {
    $extension = pathinfo($file, PATHINFO_EXTENSION);
    $basename = basename($file, ".$extension");
    $exploded_date = explode("-", $basename);

    $date = Array('day' => $exploded_date[1], // [0] is "palyvoice.com"
		  'month' => $exploded_date[2],
		  'year' => $exploded_date[3]);

    array_push($dates, $date);
  }

  return $dates;
}

function get_year_list ($dates) {
  $years = Array();

  foreach ($dates as &$date) {
    array_push($years, $date['year']);
  }

  rsort($years);
  return array_unique($years);
}

function get_month_list ($dates, $year) {
  $months = Array();

  foreach ($dates as &$date) {
    if ($date['year'] == $year) {
      array_push($months, $date['month']);
    }
  }

  rsort($months);
  return array_unique($months);
}

function get_day_list ($dates, $year, $month) {
  $days = Array();

  foreach ($dates as &$date) {
    if ($date['year'] == $year && $date['month'] == $month) {
      array_push($days, $date['day']);
    }
  }

  rsort($days);
  return array_reverse(array_unique($days));
}

function get_calendar ($dates, $publication) {
  $years = get_year_list($dates);
  
  echo "<h3 style='margin-top:0;'>".$publication."</h3>";

  foreach ($years as &$year) {
    echo "$year\n";

    $months = get_month_list($dates, $year);
    foreach ($months as &$month) {
      $timestamp = mktime(0, 0, 0, $month, 10);
      $monthName = date("F", $timestamp);
      echo "$monthName\n";

      $i = 0;
      $days = get_day_list($dates, $year, $month);
      foreach ($days as &$day) {
	echo "<a href='/".$publication."/$year/$month/$day'>$day</a> ";
	$i++;
	if ($i % 7 == 0)  echo ("\n") ;
      }
      echo "\n";
    }
    echo "\n";
  }
}

function display_from_directory ($publication) {
  $files = get_file_list($publication);
  $dates = get_date_list($files);
  get_calendar($dates, $publication);
}