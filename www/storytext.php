<?php
   $publication = $_GET['publication'];
   $day = $_GET['day'];
   $month = $_GET['month'];
   $year = $_GET['year'];
   $id = $_GET['id'];

   $filename = "$publication-$day-$month-$year";
   $json_name = "assets/$publication/$filename.json";
   $file_exists = file_exists($json_name);
   $stories = $file_exists ? json_decode(file_get_contents($json_name)) : Array();
   $found_story = NULL;
   if (count($stories) != 0) {
     foreach ($stories as $story) {
       if ($story->id == $id) $found_story = $story;
     }
   }
   $json_story = json_encode($found_story);
?>

<head>
  <title><?= $found_story->title ?></title>
  <!-- <?= "assets/$publication/$filename.json" ?> -->
  <style tyle="text/css">
    #title {
      font-size: 30px;
      font-weight: bold;
      margin-bottom: 20px;
    }
    #byline {
	font-size: 20px;
	font-style: italic;
	margin-bottom: 20px;
    }
    #body {
      white-space: pre-wrap;
      width: 600px;
    }
  </style>
  <script type="text/javascript" src="/lib/jquery-1.11.0.min.js"></script>
  <script type="text/javascript">
    $(document).ready(function () {
	var story = <?= $json_story ?>;
	if (story == null) {
	    $("#story").html("story not available...");
	}
	else {
	    $("#story #title").html(story.title);
	    $("#story #byline").html("by "+story.author);
	    $("#story #body").html(story.content);
	}
    });
  </script>
</head>

<body>
  <div id="story">
    <div id="title"></div>
    <div id="byline"></div>
    <div id="body"></div>
  </div>
</body>
