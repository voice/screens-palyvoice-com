<?php
   $publication = $_GET['publication'];
   $day = $_GET['day'];
   $month = $_GET['month'];
   $year = $_GET['year'];

   $imgpath = "/$publication/$year/$month/$day.png";
   $jsonpath = "assets/$publication/$publication-$day-$month-$year.json";
?>

<head>
  <title>Screenshots</title>

  <style>
    .single {
    float: left;
    width: 450px;
    padding: 19px 29px 29px;
    margin-bottom: 50px;
    margin-left: 30px;
    margin-top: 30px;
    background-color: #f5fff5;
    border: 1px solid #e5e5e5;
    -webkit-border-radius: 5px;
    -moz-border-radius: 5px;
    border-radius: 5px;
    -webkit-box-shadow: 0 1px 2px rgba(0,0,0,.05);
    -moz-box-shadow: 0 1px 2px rgba(0,0,0,.05);
    box-shadow: 0 1px 2px rgba(0,0,0,.05);
    }
    img {
    max-width:100%;
    margin-left: -50px;
    }
  </style>
</head>
<body>
  <div class="single">
    <pre>
      <img src="<?= $imgpath ?>"</img>
    </pre>
  </div>
  <div class="single">
      <div class="storylist">
	<code>
	  <h3>rss feed from <?= $day ?>/<?= $month ?>/<?= $year ?></h3>
	  <p>coming soon...</p>
	  <ol>
	  </ol>
	</code>
      </div>
  </div>
  <?php
     $file_exists = file_exists($jsonpath);
  ?>
  <script type="text/javascript" src="/lib/jquery-1.11.0.min.js"></script>
  <script type="text/javascript">
    $(document).ready(function () {
    	var stories = <?= $file_exists ? file_get_contents($jsonpath) : "[]" ?>;
	if (stories.length == 0) {
	    $(".storylist code p").html("stories not available...");
	}
	else {
	    stories.forEach(function (story) {
		var li = $("<li></li>");
		var b = $("<b></b>");
		var a = $("<a></a>");
		a.attr({ href: document.location+"/"+story.id }).html(story.title);
		b.append(a);
		li.append(b);
		$(".storylist code p").remove();
		$(".storylist code ol").append(li);
   	    });
	}
    });
  </script>
</body>
