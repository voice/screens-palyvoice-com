<?php
  $title = 'Archives';
  include 'header.php';
  include 'functions.php';
?>

<body>
<div class="site boxed">
   <div class="header boxed green">    
   <code><h1 style="display:inline-block">Paly Online Publication Archives</h1></code>
    <span style="margin-left:110px;"><a href="http://palyvoice.com"><img src="/lib/powered_by_voice.svg"></a></span>
   </div>
   <!--div class="pub boxed green">
      <code>
	<pre><br /><h3 style="margin-top:0;">Keep in mind:</h3></pre>
	<ol>
	  <li>Dates are dd/mm/yyyy</li>
	  <li>Project is incomplete</li>
	  <li>Project is under constant development</li>
	  <li>Project may break without warning</li>
	</ol>
      </code>
    </div-->
    <div class="pub boxed green">
      <pre>
	<?php display_from_directory("palyvoice.com"); ?>
      </pre>
    </div>
    <div class="pub boxed green">
      <pre>
	<?php display_from_directory("paly.net"); ?>
      </pre>
    </div>
    <div class="pub boxed green">
      <pre>
	<?php display_from_directory("palycampanile.org"); ?>
      </pre>
    </div>
    <div class="pub boxed green">
      <pre>
	<?php display_from_directory("verdemagazine.com"); ?>
     </pre>
     </div>

     <br class="clear">
    <div class="footer-small boxed green">
      <code>Made with love by <a href="http://bernsteinbear.com">Maxwell Bernstein</a> & <a href="http://chinstorff.com">Christopher Hinstorff</a>.</code>
    </div>
    <div class="footer-large boxed green">
      <code>
	<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="http://i.creativecommons.org/l/by-nc-sa/4.0/80x15.png" /></a><br /><span xmlns:dct="http://purl.org/dc/terms/" property="dct:title">Screenshot Archive (software)</span> by <span xmlns:cc="http://creativecommons.org/ns#" property="cc:attributionName">Maxwell Bernstein & Christopher Hinstorff</span> is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License</a>. Permissions beyond the scope of this license may be available at <a xmlns:cc="http://creativecommons.org/ns#" href="http://archives.palyvoice.com/use" rel="cc:morePermissions">http://archives.palyvoice.com/use</a>.
      </code>
    </div>
    <br class="clear">
</div>
</body>
