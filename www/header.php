<!DOCTYPE html>
<html style="min-height:100%">
<head>
  <title><?php echo $title;?></title>
  <link rel="stylesheet" type="text/css" href="lib/screens.css">
  <link rel="icon" type="img/png" href="lib/favicon.png">

  <script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-34311211-2', 'palyvoice.com');
  ga('send', 'pageview');
  </script>
</head>

<?php error_reporting(E_ALL); ?>