<style type="text/css">
  div {
    position: absolute;
    height: 400px;
    width: 400px;
    margin: auto;
    top: 0; left: 0; bottom: 0; right: 0;
  }
  div {
    text-align: justify;
    text-justify: inter-word;
    font-family:Consolas,Monaco,Lucida Console,Liberation Mono,DejaVu Sans Mono,Bitstream Vera Sans Mono,Courier New, monospace;
  }
   }
</style>
<div>
  <p>This list contains the names of organizations and/or people prohibited from using or claiming ownership to the <a href="http://archives.palyvoice.com">work</a> in any form:</p>

  <ol>
    <li>Palo Alto Unified School District</li>
    <li>United States Government</li>
  </ol>

  <p>Please note that items may be parent oranizations, and unless expressly permitted, sub-organizations of those parent organizations may not use the work in any form.</p>

  <p>Also note that anybody using or modifying the <a href="http://archives.palyvoice.com">work</a> is subject to aforementioned additional license clauses.</p>
</div>
