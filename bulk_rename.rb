=begin

Usage:
ruby bulk_rename.rb [string_from] [string_to]
(include dashes)

=end

from = ARGV[0]
to = ARGV[1]

files = Dir.glob("#{from}*")

files.each do |file|
  new = file.gsub(from, to)
  puts "renaming #{file} to #{new}"
  File.rename file, new
end
