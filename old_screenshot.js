function areSitesDone (pageStateList) {
    var state = true;
    for (site in pageStateList)
	state = state && pageStateList[site];
    return state;
}

function exitIfDone (pagesStateList) {
    console.log(JSON.stringify(pagesStateList));
    if (areSitesDone(pagesStateList)) phantom.exit();
}

var webpageModule = require('webpage');
var color = require('bash-color');

var phantomPageList = {};
var sitesToScreenshot = {
    palyvoice: 'http://palyvoice.com',
//    campanile: 'http://palycampanile.org'
};
var pagesDone = {};

for (var siteName in sitesToScreenshot) {
    pagesDone[siteName] = false;
    phantomPageList[siteName] = webpageModule.create();
    phantomPageList[siteName].open(sitesToScreenshot[siteName]);
    console.log(color.cyan('adding site ')+siteName);
    phantomPageList[siteName].onLoadFinished = function () {
	var now = new Date();
	//                                         vvvvvvvvvvvvvv zero-indexed

	console.log(color.green('saving screenshot ')+fileName);
	//phantomPageList[siteName].render(fileName);
	pagesDone[siteName] = true;
    };
}

setInterval(exitIfDone(pagesDone), 500);
